import tkinter as tk

from .entry_with_label import EntryWithLabel
from .float_with_label import EntryFloatWithLabel


class EntryNameAmount(tk.Frame):
    def __init__(self, root, **kw):
        super().__init__(root, **kw)

        self.entry_name = EntryWithLabel(self, "Name:", 10)
        self.entry_amount = EntryFloatWithLabel(self, "Amount:", 5, fallback_value=1)
        self.entry_name.pack(side=tk.LEFT)
        self.entry_amount.pack(side=tk.LEFT)

    def get_name(self) -> str:
        return self.entry_name.get()

    def set_name(self, new_name):
        return self.entry_name.set(new_name)

    def get_amount(self) -> int:
        return self.entry_amount.get()

    def set_amount(self, amount):
        return self.entry_amount.set(amount)

    def is_empty(self):
        return self.get_name() == ""
